# Greeting Bot

## Dependencies
- [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot)
    - `pip3 install python-telegram-bot`

## Installation
1. Copy config.json.template to config.json
2. Insert your token and a greeting message

## Variables
The bot can insert custom values into the msg.

| Name         |                   |
|--------------|-------------------|
| $FIRSTNAME   | message.firstname |
